Feature: Login

Background: 
	Given User Launch browser
	When User opens URL "http://admin-demo.nopcommerce.com/login"

@sanity
Scenario: Successful Login with Valid Credentials
#Given User Launch browser
#When User opens URL "http://admin-demo.nopcommerce.com/login"
And User enters Email as "admin@yourstore.com" and Password as "admin"
And Click on Login
Then Page Title Should be "Dashboard / nopCommerce administration"
When User click on Log out link
Then Page Title Should be "Your store. Login"


@regression
Scenario Outline: Login Data Driven
#Given User Launch browser
#When User opens URL "http://admin-demo.nopcommerce.com/login"
And User enters Email as "<email>" and Password as "<password>"
And Click on Login
Then Page Title Should be "Dashboard / nopCommerce administration"
When User click on Log out link
Then Page Title Should be "Your store. Login"

Examples:
| email 							 | password |
| admin@yourstore.com  | admin    |
| admin1@yourstore.com | admin123 |
