Feature: Customers

Background:
	Given User Launch browser
	When User opens URL "http://admin-demo.nopcommerce.com/login"

@sanity
Scenario: Add a new Customer
#	Given User Launch browser
#	When User opens URL "http://admin-demo.nopcommerce.com/login"
	And User enters Email as "admin@yourstore.com" and Password as "admin"
	And Click on Login
	Then User can view Dashboard
	When User clicks on customers Menu
	And Click on customers Menu Item
	And Click on Add new button
	Then User can view Add new customer page 
	When User enters customer info
	And Click on Save button
	Then User can view confirmation message "The new customer has been added successfully."

@regression	
Scenario: Search Customer by EMailID
#	Given User Launch browser
#	When User opens URL "http://admin-demo.nopcommerce.com/login"
	And User enters Email as "admin@yourstore.com" and Password as "admin"
	And Click on Login
	Then User can view Dashboard
	When User clicks on customers Menu
	And Click on customers Menu Item 
	And Enter customer EMail
	When Click on search button
	Then User should found a email in search table

@regression	
Scenario: Search Customer by Name
#	Given User Launch browser
#	When User opens URL "http://admin-demo.nopcommerce.com/login"
	And User enters Email as "admin@yourstore.com" and Password as "admin"
	And Click on Login
	Then User can view Dashboard
	When User clicks on customers Menu
	And Click on customers Menu Item 
	And Enter Customer FirstName
	And Enter Customer LastName
	When Click on search button
	Then User should found a name in search table