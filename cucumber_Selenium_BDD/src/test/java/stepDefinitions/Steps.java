package stepDefinitions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.codehaus.plexus.util.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.After;
import io.cucumber.java.AfterAll;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeAll;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.AddCustomerPage;
import pageObjects.LoginPage;
import pageObjects.SearchCustomerPage;

public class Steps extends BaseClass{

	@BeforeAll
	public static void before_all() {
		System.out.println("We are inside BeforeAll");
	}
	
	@AfterAll
	public static void after_all() {
		System.out.println("We are inside AfterAll");
	}
	
	
	@Before
	public void setup() throws IOException {
		
		configProp = new Properties();
		//Reading properties file
		FileInputStream fis = new FileInputStream("config.properties");
		configProp.load(fis);
		logger = Logger.getLogger("cucumber_Selenium_BDD"); //Added Logger
		PropertyConfigurator.configure("log4j.properties"); //Added Logger
		
		String br = configProp.getProperty("browser");
		
		if(br.equalsIgnoreCase("chrome")) {
			ChromeOptions o = new ChromeOptions();
			o.addArguments("--disable-notifications");
			System.setProperty("webdriver.chrome.driver", configProp.getProperty("chromePath"));
			driver = new ChromeDriver(o);
		}
		else if(br.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", configProp.getProperty("geckoPath"));
			driver = new FirefoxDriver();
		}else if(br.equalsIgnoreCase("edge")) {
			// edge browser details
		}
		else {
			Assert.assertTrue("Incorrect browser name", false);
		}
		

	}
	
	
	@Given("User Launch browser")
	public void user_Launch_Browser() {
		// Write code here that turns the phrase above into concrete actions
	
		logger.info("**********Launching Browser**********");
		lp = new LoginPage(driver);

	}

	@When("User opens URL {string}")
	public void user_opens_URL(String url) {
		// Write code here that turns the phrase above into concrete actions
		logger.info("**********Opening URL**********");
		driver.get(url);
	}

	@When("User enters Email as {string} and Password as {string}")
	public void user_enters_Email_as_and_Password_as(String email, String password) {
		// Write code here that turns the phrase above into concrete actions
		logger.info("**********Providing Login details**********");
		lp.setUsername(email);
		lp.setPassword(password);
	}

	@When("Click on Login")
	public void click_on_Login() throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		logger.info("**********Stating Login**********");
		lp.clickLogin();
		Thread.sleep(5000);
	}

	@When("User click on Log out link")
	public void click_Logout_Link() throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		logger.info("**********Clicking Logout link**********");
		lp.clickLogout();
		Thread.sleep(5000);
	}

	@Then("Page Title Should be {string}")
	public void page_Title_Should_be(String expectedTitle) {
		// Write code here that turns the phrase above into concrete actions
		if (driver.getPageSource().contains("Login was unsuccessful")) {
			driver.close();
			Assert.assertTrue(false);

		} else {
			String actualTitle = driver.getTitle();
			Assert.assertEquals(expectedTitle, actualTitle);
		}
	}

//	@Then("Close browser")
	@After()
	public void teardown(Scenario scenario) throws IOException {
		// Write code here that turns the phrase above into concrete actions
		// driver.close();
		//Take screenshot it sceanrio failed
		if(scenario.isFailed()) {
			TakesScreenshot screenshot = (TakesScreenshot)driver;
			File srcFile = screenshot.getScreenshotAs(OutputType.FILE);
			File destFile = new File("./Screenshot/"+scenario.getId()+".png");
			FileUtils.copyFile(srcFile,destFile );
		}
		logger.info("**********Closing browser**********");
		driver.quit();
	}
	
	
	//Customer feature  step definitions..............................
	
	@Then("User can view Dashboard")
	public void user_can_view_dashboard() {
	    // Write code here that turns the phrase above into concrete actions
		
		addCust = new AddCustomerPage(driver);
		Assert.assertEquals("Dashboard / nopCommerce administration", addCust.getPageTitle());
	    
	}
	@When("User clicks on customers Menu")
	public void user_clicks_on_customers_menu() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	   Thread.sleep(1000);
	   addCust.clickOnCustomersMenu();
	}
	
	@When("Click on customers Menu Item")
	public void click_on_customers_menu_item() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		   Thread.sleep(1000);
		   addCust.clickOnCustomersMenuItem();
		   searchCust = new SearchCustomerPage(driver);
	}
	@When("Click on Add new button")
	public void click_on_add_new_button() throws InterruptedException {
		logger.info("**********Adding new customer**********");
	    Thread.sleep(2000);
	    addCust.clickOnAddNew();
	}
	
	@Then("User can view Add new customer page")
	public void user_can_view_add_new_customer_page() {
	    // Write code here that turns the phrase above into concrete actions
		addCust = new AddCustomerPage(driver);
		Assert.assertEquals("Add a new customer / nopCommerce administration", addCust.getPageTitle());
	}
	
	@When("User enters customer info")
	public void user_enters_customer_info() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		logger.info("**********Adding new customer details**********");
	    String email = this.randomstring()+"@gmail.com";
	    addCust.setEmail(email);
	    addCust.setPassword("test123");
	    addCust.setFirstName("Lalit");
	    addCust.setLastName("Dixit");
	    addCust.selectGender("Male");
	    addCust.setDate("1/27/1989");
	    addCust.setCompanyName("Oracle");
	    addCust.isTaxExempted(true);
	  //  addCust.selectNewsletter("Your store name");
	  //  addCust.selectCustomerRoles("Guests");
	    addCust.selectVendors("Vendor 1");
	   // addCust.isActive(true);
	    addCust.addComments("adding new user");
	    
	}
	@When("Click on Save button")
	public void click_on_save_button() {
	    // Write code here that turns the phrase above into concrete actions
		logger.info("**********Saving new customer**********");
	    addCust.clickSave();
	}
	@Then("User can view confirmation message {string}")
	public void user_can_view_confirmation_message(String string) {
	    // Write code here that turns the phrase above into concrete actions
	    Assert.assertTrue(driver.findElement(By.tagName("body")).getText()
	    		.contains("The new customer has been added successfully."));
	}
	
	
	// Steps for searching customer using EMailID- ----------------
	
	@When("Enter customer EMail")
	public void enter_customer_e_mail() {
	    // Write code here that turns the phrase above into concrete actions
		logger.info("**********Starting customer search using email**********");
	    searchCust.setEmail("victoria_victoria@nopCommerce.com");
	}
	
	@When("Click on search button")
	public void click_on_search_button() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		logger.info("**********Clicking search button**********");
	    searchCust.clickSearch();
	    Thread.sleep(3000);
	}
	@Then("User should found a email in search table")
	public void user_should_found_a_email_in_search_table() {
	    // Write code here that turns the phrase above into concrete actions
		logger.info("**********Validating the search customer results**********");
	    boolean status = searchCust.searchCustomerByEmail("victoria_victoria@nopCommerce.com");
	    Assert.assertTrue(status);
	}
	
	@When("Enter Customer FirstName")
	public void enter_customer_first_name() {
	    // Write code here that turns the phrase above into concrete actions
		logger.info("**********SStarting customer search using First name**********");
	    searchCust.setFirstName("Victoria");
	}
	@When("Enter Customer LastName")
	public void enter_customer_last_name() {
	    // Write code here that turns the phrase above into concrete actions
		logger.info("**********SStarting customer search using Last name**********");
		 searchCust.setLastName("Terces");
	}
	
	@Then("User should found a name in search table")
	public void user_should_found_a_name_in_search_table() {
	    // Write code here that turns the phrase above into concrete actions
		logger.info("**********Validating the search customer results**********");
	    boolean status = searchCust.searchCustomerByName("Victoria Terces");
	    Assert.assertTrue(status);
	}


}
