package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AddCustomerPage {

	public WebDriver ldriver;

	public AddCustomerPage(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(ldriver, this);
	}

	By lnkCustomers_menu = By.xpath("//a[@href='#']/p[contains(text(),'Customers')]");
	By lnkCustomers_menuItem = By.xpath("//a[@href='/Admin/Customer/List']/p[contains(text(),'Customers')]");
	By btnAddNew = By.xpath("//a[@href='/Admin/Customer/Create']");
	By txtEmail = By.id("Email");
	By txtPassword = By.id("Password");
	By txtFirstName = By.id("FirstName");
	By txtLastName = By.id("LastName");
	By radioGenderMale = By.id("Gender_Male");
	By radioGenderFemale = By.id("Gender_Female");
	By txtCompanyName = By.id("Company");
	By checkTaxExemption = By.id("IsTaxExempt");
	By listItemVendors = By.id("VendorId");
	By fieldCustomerRoles = By.xpath("//ul[@id='SelectedCustomerRoleIds_taglist']/parent::div");
	By listItemCustomerRoles = By.id("SelectedCustomerRoleIds");
	By fieldNewsletter = By.xpath("//ul[@id='SelectedNewsletterSubscriptionStoreIds_taglist']/parent::div");
	By listItemNewsletter = By.id("SelectedNewsletterSubscriptionStoreIds");
	By checkActive = By.id("Active");
	By txtComment = By.id("AdminComment");
	By txtDOB = By.id("DateOfBirth");
	By btnSave = By.name("save");

	public String getPageTitle() {
		return ldriver.getTitle();
	}

	public void clickOnCustomersMenu() {
		ldriver.findElement(lnkCustomers_menu).click();
	}

	public void clickOnCustomersMenuItem() {
		ldriver.findElement(lnkCustomers_menuItem).click();
	}

	public void clickOnAddNew() {
		ldriver.findElement(btnAddNew).click();
	}

	public void setEmail(String emailValue) {
		WebElement emailField = ldriver.findElement(txtEmail);
		emailField.clear();
		emailField.sendKeys(emailValue);
	}

	public void setPassword(String passwordValue) {
		WebElement passwordField = ldriver.findElement(txtPassword);
		passwordField.clear();
		passwordField.sendKeys(passwordValue);

	}

	public void setFirstName(String name) {
		WebElement fNameField = ldriver.findElement(txtFirstName);
		fNameField.clear();
		fNameField.sendKeys(name);
	}

	public void setLastName(String name) {
		WebElement lNameField = ldriver.findElement(txtLastName);
		lNameField.clear();
		lNameField.sendKeys(name);
	}

	public void selectGender(String gender) {
		if (gender.equalsIgnoreCase("male")) {
			ldriver.findElement(radioGenderMale).click();
		} else if (gender.equalsIgnoreCase("female")) {
			ldriver.findElement(radioGenderFemale).click();
		} else {
			System.out.println("Incorrect gender value passed!");
		}
	}

	public void setDate(String date) {
		ldriver.findElement(txtDOB).sendKeys(date);
	}

	public void setCompanyName(String name) {
		WebElement companyField = ldriver.findElement(txtCompanyName);
		companyField.clear();
		companyField.sendKeys(name);
	}

	public void isTaxExempted(Boolean flag) {
		if (flag) {
			ldriver.findElement(checkTaxExemption).click();
		}
	}

	public void selectNewsletter(String optionValue) {
		ldriver.findElement(fieldNewsletter).click();
		Select slt = new Select(ldriver.findElement(listItemNewsletter));
		slt.selectByVisibleText(optionValue);
	}

	public void selectCustomerRoles(String optionValue) {
		ldriver.findElement(fieldCustomerRoles).click();
		Select slt = new Select(ldriver.findElement(listItemCustomerRoles));
		slt.selectByVisibleText(optionValue);
	}

	public void selectVendors(String optionValue) {
		Select slt = new Select(ldriver.findElement(listItemVendors));
		slt.selectByVisibleText(optionValue);
	}

	public void isActive(Boolean flag) {
		if (flag) {
			ldriver.findElement(checkActive).click();
		}
	}

	public void addComments(String comm) {
		WebElement commentField = ldriver.findElement(txtComment);
		commentField.clear();
		commentField.sendKeys(comm);
	}
	
	public void clickSave() {
		ldriver.findElement(btnSave).click();
	}

}
