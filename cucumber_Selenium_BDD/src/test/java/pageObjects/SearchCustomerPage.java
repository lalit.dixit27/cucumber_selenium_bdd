package pageObjects;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import utilities.WaitHelper;

public class SearchCustomerPage {

	public WebDriver ldriver;
	public WaitHelper waithelper;

	public SearchCustomerPage(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(ldriver, this);
		waithelper = new WaitHelper(ldriver);
	}

	@FindBy(how = How.ID, using = "SearchEmail")
	@CacheLookup
	WebElement txtEmail;

	@FindBy(how = How.ID, using = "SearchFirstName")
	@CacheLookup
	WebElement txtFirstName;

	@FindBy(how = How.ID, using = "SearchLastName")
	@CacheLookup
	WebElement txtLastName;

	@FindBy(how = How.ID, using = "SearchMonthOfBirth")
	@CacheLookup
	WebElement drpDOBMonth;

	@FindBy(how = How.ID, using = "SearchDayOfBirth")
	@CacheLookup
	WebElement drpDOBDay;

	@FindBy(how = How.ID, using = "SearchCompany")
	@CacheLookup
	WebElement txtCompany;

	@FindBy(how = How.ID, using = "search-customers")
	@CacheLookup
	WebElement btnSearchCustomers;

	@FindBy(how = How.ID, using = "customers-grid_wrapper")
	@CacheLookup
	WebElement searchTable;

	@FindBy(how = How.XPATH, using = "//div[@id='customers-grid_wrapper']//tbody/tr")
	@CacheLookup
	List<WebElement> tableRows;

	@FindBy(how = How.XPATH, using = "//div[@id='customers-grid_wrapper']//tbody/tr/td")
	@CacheLookup
	List<WebElement> tableColumns;

	public void setEmail(String email) {
		txtEmail.clear();
		txtEmail.sendKeys(email);
	}

	public void setFirstName(String fName) {
		txtFirstName.clear();
		txtFirstName.sendKeys(fName);

	}

	public void setLastName(String lName) {
		txtLastName.clear();
		txtLastName.sendKeys(lName);

	}

	public void selectDOBMonth(String monthName) {
		Select slt = new Select(drpDOBMonth);
		slt.selectByVisibleText(monthName);
	}

	public void selectDOBDay(String dayvalue) {
		Select slt = new Select(drpDOBDay);
		slt.selectByVisibleText(dayvalue);
	}

	public void setCompany(String companyName) {
		txtCompany.clear();
		txtCompany.sendKeys(companyName);
	}

	public void clickSearch() {
		btnSearchCustomers.click();
	}

	public int getNoOfRows() {
		return tableRows.size();
	}

	public int getNoOfColumns() {
		return tableColumns.size();
	}

	public boolean searchCustomerByEmail(String email) {
		boolean flag = false;
		String actualEmail = null;
		for (int i = 1; i <= this.getNoOfRows(); i++) {
			actualEmail = searchTable
					.findElement(By.xpath("//div[@id='customers-grid_wrapper']//tbody/tr[" + i + "]/td[2]")).getText();
			System.out.println(actualEmail);
			if (email.equals(actualEmail)) {
				flag = true;
			} else {
				return false;
			}

		}
		return flag;
	}
	
	public boolean searchCustomerByName(String name) {
		boolean flag = false;
		String actualName = null;
		for (int i = 1; i <= this.getNoOfRows(); i++) {
			actualName = searchTable
					.findElement(By.xpath("//div[@id='customers-grid_wrapper']//tbody/tr[" + i + "]/td[3]")).getText();
			System.out.println(actualName);
			if (name.equals(actualName)) {
				flag = true;
			} else {
				return false;
			}

		}
		return flag;
	}
}
